# -*- coding: utf-8 -*-
# Copyright 2020 Cardiff University <macleoddm@cardiff.ac.uk>

import sys
import warnings

import nds2

NDS2_SERVER = "losc-nds.ligo.org"

# connect (but 'skip' on connection errors)
try:
    conn = nds2.connection(NDS2_SERVER)
except RuntimeError as exc:
    warnings.warn(
        "caught {}: {} [skipping]".format(type(exc).__name__, str(exc)),
    )
    sys.exit(77)

# query for some channels and print the names
for chan in conn.find_channels('H1:PEM-CS_ACC*'):
    print(chan.name, chan.sample_rate)
