#!/bin/bash
#
# IGWN Conda Distribution tests for LALApps
#

python -c "
import lalapps
import lalapps.cosmicstring
import lalapps.inspiral
"

# create fake data
framecpp_sample

# random collection of scripts
lalapps_tconvert
lalapps_chirplen --m1 1.4 --m2 1.4 --flow 30
lalapps_frextr test.gwf Z-ilwd_test_frame-600000000-1.gwf Z0:RAMPED_INT_2U_1
lalapps_frinfo Z-ilwd_test_frame-600000000-1.gwf
lalapps_frjoin --verbose --output test.gwf Z-ilwd_test_frame-600000000-1.gwf
lalapps_frread Z-ilwd_test_frame-600000000-1.gwf Z0:RAMPED_INT_2U_1
lalapps_path2cache <<< Z-ilwd_test_frame-600000000-1.gwf
test "$(lalapps_tconvert --utc -f "%Y-%m-%d %H:%M:%S" 800000000)" == "2005-05-13 06:13:07"
test "$(lalapps_tconvert Fri, 13 May 2005 06:13:07 +0000)" -eq 800000000
