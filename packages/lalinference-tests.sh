#!/bin/bash
#
# IGWN Conda Distribution tests for LALInference
#

# liblalinference
pkg-config --print-errors --libs lalinference

# python-lalinference

# lalinference
lalinference_bench --psdlength 1000 --psdstart 1 --seglen 8 --srate 4096 --trigtime 0 --ifo H1 --H1-channel LALSimAdLIGO --H1-cache LALSimAdLIGO --dataseed 1324 --Niter 10 --fix-chirpmass 1.21
