#!/bin/bash
#
# IGWN Conda Distribution tests for Bilby
#

# smoke tests
bilby_plot --help
bilby_result --help

# basic test
python -c "
from bilby.gw.detector import PowerSpectralDensity
print(PowerSpectralDensity.from_aligo().psd_array)
"
