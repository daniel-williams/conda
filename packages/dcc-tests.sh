#!/bin/bash
#
# IGWN Conda Distribution tests for DCC
#

# sanity check the command-line interface
dcc --help

# query for metadata from public version of Frame Spec
python -c "
import sys
import tempfile
import traceback
from pathlib import Path

import requests

from dcc import (
    records,
    sessions,
)

arch = records.DCCArchive('.')
sess = sessions.default_session(authenticated=False)

try:
    record = arch.fetch_record('T970130', session=sess)
except requests.RequestException:
    # something failed with the connection, return code 77 so pytest skips
    traceback.print_exc()
    sys.exit(77)


with tempfile.TemporaryDirectory() as tmpdir:
    tmp = Path(tmpdir) / 'tmp.toml'
    record.write(tmp)
    print('')
    print(tmp.read_text())
"
