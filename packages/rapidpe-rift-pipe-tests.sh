#!/bin/bash
#
# IGWN Conda Distribution tests for rapidpe-rift-pipe
#

# run test suite from pytest
python -m pytest \
  -ra \
  --cache-clear \
  --no-header \
  --pyargs \
  rapidpe_rift_pipe

# run --help for each of the scripts
rapidpe-rift-pipe --help
