#!/bin/bash
#
# IGWN Conda Distribution tests for GstLAL Calibration
#

python -c "
import gstlalcalibration.calibhandler
import gstlalcalibration.calibration_parts
"

gstlal_calibration_aggregator --help
gstlal_compute_strain --help
#gstlal_correct_strain --help  <-- https://git.ligo.org/Calibration/gstlal-calibration/-/issues/29
