#!/bin/bash
#
# IGWN Conda Distribution tests for Asimov (ligo-asimov)
#

# test modules
python -c "
import asimov
"

# test entry points
locutus --help
olivaw --help
asimov --help
