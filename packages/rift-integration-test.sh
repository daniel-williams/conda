#!/bin/bash
#
# copied from https://github.com/oshaughn/research-projects-RIT/blob/fa7cc6db997db3b0b6bde7d9ebd2971a110a95e9/.travis/test-run.sh#L5-L7 with permission from Richard

git clone --depth 1 https://github.com/oshaughn/ILE-GPU-Paper.git repo
cd repo/demos/
make test_workflow_batch_gpu_lowlatency V=1 VERBOSE=1
