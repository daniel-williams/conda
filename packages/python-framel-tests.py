# -*- coding: utf-8 -*-
# Copyright 2020 Cardiff University <macleoddm@cardiff.ac.uk>

import os
import subprocess

import pytest

import numpy
from numpy.testing import assert_array_equal

import framel


@pytest.fixture
def sample_data(tmp_path):
    curdir = os.getcwd()
    os.chdir(tmp_path)
    try:
        subprocess.check_call(["framecpp_sample"])
        yield tmp_path / "Z-ilwd_test_frame-600000000-1.gwf"
    finally:
        os.chdir(curdir)


def test_read_write(sample_data, tmp_path):
    # test read
    data, start, offset, rate, xunit, yunit = framel.frgetvect1d(
        str(sample_data),
        "Z0:RAMPED_REAL_4_1",
    )
    assert data.dtype == numpy.dtype("float32")
    assert start == 600000000.
    assert rate == 1.  # this should be 1024...

    # test write
    tmpgwf = str(tmp_path / "test2.gwf")
    framel.frputvect(
        tmpgwf,
        [{
           'name': 'A1:TEST',
           'data': data,
           'start': start + offset,
           'dx': rate,
           'x_unit': xunit,
           'y_unit': yunit,
        }],
    )

    # test read again
    data2, start2, offset2, rate2, xunit2, yunit2 = framel.frgetvect1d(
        tmpgwf,
        "A1:TEST",
    )
    assert_array_equal(data, data2)
    assert start == start2
    assert offset == offset2
    assert rate == rate2
    assert xunit == xunit2
    assert yunit == yunit2
