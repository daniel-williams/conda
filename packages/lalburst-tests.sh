#!/bin/bash
#
# IGWN Conda Distribution tests for LALBurst
#

# liblalburst
pkg-config --print-errors --libs lalburst

# python-lalburst
python -c "
import lalburst.SimBurstUtils
import lalburst.SnglBurstUtils
import lalburst.binjfind
import lalburst.bucluster
import lalburst.burca
import lalburst.burca_tailor
import lalburst.cafe
import lalburst.calc_likelihood
import lalburst.cs_gamma
import lalburst.date
import lalburst.packing
import lalburst.snglcluster
import lalburst.snglcoinc
import lalburst.timeslides
"

# lalburst
lalburst_version
