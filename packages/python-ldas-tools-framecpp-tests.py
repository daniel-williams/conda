# -*- coding: utf-8 -*-
# Copyright 2020 Cardiff University <macleoddm@cardiff.ac.uk>

import os
import subprocess

import pytest

import numpy

from LDAStools import frameCPP


@pytest.fixture
def sample_data(tmp_path):
    curdir = os.getcwd()
    os.chdir(tmp_path)
    try:
        subprocess.check_call(["framecpp_sample"])
        yield tmp_path / "Z-ilwd_test_frame-600000000-1.gwf"
    finally:
        os.chdir(curdir)


def test_frgetvect1d(sample_data):
    stream = frameCPP.IFrameFStream(str(sample_data))
    adc = stream.ReadFrAdcData(0, "Z0:RAMPED_REAL_4_1")
    assert adc.GetSampleRate() == 1024.
