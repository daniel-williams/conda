#!/bin/bash
#
# IGWN Conda Distribution tests for INSERT_PACKAGE_NAME
#

# -- configurables

# name of package (according to Python metadata)
PKG_NAME="bilby_tgr"

# path of test directory
TEST_PATH="tests/"

# other paths from tarball needed by tests (include leading */ for each entry)
EXTRA_PATHS=""

# test command to run
TEST_CMD="python -m pytest -ra --cache-clear --no-header"

# -- versioning

# get package version from metadata
VERSION=$(python -c "
from importlib.metadata import version
print(version('${PKG_NAME}'))
")

# -- download and run tests

# download and unpackage the tarball
URL="https://pypi.io/packages/source/${PKG_NAME::1}/${PKG_NAME/_/-}/${PKG_NAME}-${VERSION}.tar.gz"
curl -Ls ${URL} | \
tar \
  -x \
  -z \
  -f - \
  --strip-components=1 $(test $(uname) = "Linux" && echo "--wildcards") \
  "*/${TEST_PATH}" \
  ${EXTRA_PATHS} \
|| {
  echo "download failed, skipping...";
  exit 77;
}

# run tests
${TEST_CMD} ${TEST_PATH} "$@"
