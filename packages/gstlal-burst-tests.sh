#!/bin/bash
#
# IGWN Conda Distribution tests for GstLAL Burst
#

export GSTLAL_FIR_WHITEN=0
export TMPDIR=${TMPDIR:-/tmp}

# check scripts
gstlal_cherenkov_burst --help
gstlal_cherenkov_calc_likelihood --help
gstlal_cherenkov_calc_rank_pdfs --help
gstlal_cherenkov_inj --help
gstlal_cherenkov_plot_rankingstat --help
gstlal_cherenkov_plot_summary --help
gstlal_cherenkov_zl_rank_pdfs --help
gstlal_cs_triggergen --help
gstlal_impulse_inj --help

# check Gst plugin
gst-inspect-1.0 gstlalburst
