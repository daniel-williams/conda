#!/bin/sh
#
# IGWN Conda Distribution test for PyCBC
#

set -e

VERSION=$(
conda list pycbc --json | \
python -c "import json, sys; print(json.load(sys.stdin)[0]['version'])"
)

EXAMPLE=$1
shift 1 || EXAMPLE="waveform/match_waveform.py"

URL=https://raw.githubusercontent.com/gwastro/pycbc/v${VERSION}/examples/${EXAMPLE}
SCRIPT=$(basename ${URL})

curl \
	--fail \
	--location \
	--output ${SCRIPT} \
	--silent \
	--show-error \
	${URL} \
|| {
    echo "download failed, skipping..." 2>&1;
    exit 77;
}

python ${SCRIPT}
