# -*- coding: utf-8 -*-
# Copyright 2020 Cardiff University <macleoddm@cardiff.ac.uk>

"""Run integration tests for GWpy
"""

__author__ = "Duncan Macleod <duncan.macleod@ligo.org>"

import socket
import tempfile

import pytest

from matplotlib import use
use('agg')  # noqa

from gwpy.timeseries import TimeSeries


# -- test timeseries plots ----------------------

def test_timeseries_plot():
    try:
        data = TimeSeries.fetch_open_data('H1', 1126259446, 1126259478)
    except socket.timeout as exc:
        pytest.skip(str(exc))
    qspecgram = data.q_transform(outseg=(1126259462.2, 1126259462.5))
    plot = qspecgram.plot(figsize=[8, 4])
    ax = plot.gca()
    ax.set_xscale('seconds')
    ax.set_yscale('log')
    ax.set_ylim(20, 500)
    ax.set_ylabel('Frequency [Hz]')
    ax.grid(True, axis='y', which='both')
    ax.colorbar(cmap='viridis', label='Normalized energy')
    with tempfile.TemporaryFile() as tmp:
        plot.save(tmp, format="png")
