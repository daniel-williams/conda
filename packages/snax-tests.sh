#!/bin/bash
#
# IGWN Conda Distribution tests for snax
#

# config for this package
PKG_NAME="snax"
PKG_IMPORT_NAME="${PKG_NAME/-/_}"
TESTS_DIR="tests"

# download the original tarball which includes the tests
VERSION=$(python -c "import ${PKG_IMPORT_NAME}; print(${PKG_IMPORT_NAME}.__version__)")
URL="https://software.igwn.org/sources/source/${PKG_NAME}-${VERSION}.tar.gz"
curl -Ls ${URL} | tar -xzf - --strip-components=1 $(test $(uname) = "Linux" && echo "--wildcards") "*/${TESTS_DIR}" || {
	echo "download failed, skipping...";
	exit 77;
}

# run pytest on the tests dir
python -m pytest \
	-ra \
	-v \
	--cache-clear \
	--no-header \
	${TESTS_DIR}/
