#!/bin/bash
#
# IGWN Conda Distribution tests for cds-crtools
#

# python modules
python -c "
import awg
import awgbase
import foton
"

# executables
diaggui -h
foton -h
