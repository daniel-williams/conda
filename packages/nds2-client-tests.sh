#!/bin/bash
# Copyright 2020 Cardiff University <macleoddm@cardiff.ac.uk>
#
# IGWN Conda Distribution tests for the NDS2 Client
#

set -ex

NDS2_SERVER="losc-nds.ligo.org"

# list channels
nds_query -n ${NDS2_SERVER} -l "H1:PEM-CS_ACC*"

# list available for one channel
nds2_channel_source -n ${NDS2_SERVER} -a "H1:PEM-CS_ACC_HAM6_OMC_Z_DQ"
