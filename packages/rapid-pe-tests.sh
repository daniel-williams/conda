#!/bin/bash
#
# IGWN Conda Distribution tests for rapid-pe
#

# run test suite from pytest
python -m pytest \
  -ra \
  --cache-clear \
  --no-header \
  --pyargs \
  rapid_pe.tests

# run --help for each of the scripts
rapidpe_calculate_overlap --help
rapidpe_compute_intrinsic_fisher --help
rapidpe_compute_intrinsic_grid --help
rapidpe_create_event_dag --help
rapidpe_integrate_extrinsic_likelihood --help
rapidpe_triangulation --help
