#!/bin/bash
#
# IGWN Conda Distribution tests for ligo-raven
#

# test scripts
raven_calc_signif_gracedb --help
raven_coinc_far --help
raven_query --help
raven_search --help
raven_skymap_overlap --help
