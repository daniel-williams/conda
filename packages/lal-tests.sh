#!/bin/bash
#
# IGWN Conda Distribution tests for LAL
#

# liblal
pkg-config --modversion lal

# python-lal
python -c "
import lal.antenna
import lal.gpstime
import lal.rate
import lal.series
import lal.utils

from lal import LIGOTimeGPS
assert LIGOTimeGPS(1) + LIGOTimeGPS(2) == 3
"

# lal
lal_version
lal_simd_detect
