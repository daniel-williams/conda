#!/bin/bash
#
# IGWN Conda Distribution tests for gpstime
#

# smoke tests
gpstime "Jan 1"
gpstime 1261872018

# unit tests
python -m unittest discover gpstime.test
