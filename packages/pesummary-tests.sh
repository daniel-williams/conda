#!/bin/bash
#
# IGWN Conda Distribution tests for PESummary
#

# set XDG_CACHE_DIR to redirect pesummary logs
export XDG_CACHE_DIR="$(pwd)/.cache"

# run the pytest suite
python -m pytest \
	-ra \
	--verbose \
	--pyargs pesummary.tests.read_test \
;
