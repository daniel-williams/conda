#!/bin/bash
# Copyright 2020 Cardiff University <macleoddm@cardiff.ac.uk>
#
# IGWN Conda Distribution tests for ligo-proxy-utils
#

# only the computing account pipelines get the kerberos ticket
klist -s || {
    echo "no valid kerberos ticket to use" 1>&2;
    exit 77;
}

# sanity checks
ligo-proxy-init --version
ligo-proxy-init --help

# check that we can generate an X.509 credential from the kerberos tgt
export X509_USER_PROXY="$(pwd)/x509"
ligo-proxy-init --kerberos --debug || {
    echo "ligo-proxy-init --kerberos failed, skipping..." 2>&1
    exit 77;
}
ligo-proxy-init --destroy --debug
