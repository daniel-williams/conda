#!/bin/bash
#
# IGWN Conda Distribution tests for LALInspiral
#

# liblalinspiral
pkg-config --print-errors --libs lalinspiral

# python-lalinspiral
python -c "
import lalinspiral
import lalinspiral.inspinjfind
import lalinspiral.sbank
import lalinspiral.thinca
"

# lalinspiral
lalinspiral_version
