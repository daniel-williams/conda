# -*- coding: utf-8 -*-
#
# IGWN Conda Distribution tests for cweqgen
#

import sys

import pytest

from cweqgen import equations


def test_equation():
    eqh0 = equations("h0")
    assert eqh0().value == pytest.approx(4.229e-26, rel=1e-3)


if __name__ == "__main__":
    sys.exit(pytest.main(args=[__file__] + sys.argv[1:]))
