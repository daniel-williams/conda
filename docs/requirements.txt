# base
mkdocs

# theme
mkdocs-material-igwn

# extensions
pymdown-extensions >=7.0

# scripts
pyyaml
tabulate
