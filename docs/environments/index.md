# Environments

The IGWN Conda Distribution includes a number of environments, covering
different versions of Python, and _testing_, _staging_, and _stable_ package
lists.
You can use these environments either directly from the CVMFS
(see [_Using pre-built environments with CVMFS_](../usage.md#cvmfs)), or by
[installing them manually](../usage.md#local) after downloading the relevant
YAML files (you can find download links on the details page for each environment).

## Default environments {: #default }

There are three Default Environments which are recommended for most users:

### igwn {: #igwn }

The [`igwn`](./igwn.md) environment represents the latest _stable_ set of
packages and versions that have been approved for use by the IGWN
[Software Change Control Board](https://computing.docs.ligo.org/sccb/).

This environment is recommended for 'everyday' use and will only change
when a new release is tagged and announced.

!!! tip "Production IGWN analyses should use a Tagged environment from CVMFS"

    To improve the reproducibility of results, all IGWN production analyses
    using the IGWN Conda Distribution in CVMFS should use a
    [Tagged environment](#tagged) that is fixed on creation.

    The `igwn` environment in CVMFS is a symbolic link to a tagged
    environment, where the link is updated whenever a new release is created.
    To unwrap the current `igwn` environment to get the tagged
    environment that it is linked to, you can use `readlink`, for example:

    ```console
    $ conda activate igwn
    $ echo ${CONDA_PREFIX}
    /cvmfs/software.igwn.org/conda/envs/igwn
    $ readlink -f "${CONDA_PREFIX}"
    /cvmfs/software.igwn.org/conda/envs/igwn-py39-20220317
    ```

    In this example all production analyses should explicitly use the
    `igwn-py39-20220317` environment.

### igwn-staging {: #igwn-staging }

The [`igwn-staging`](./igwn-staging.md) environment represents the latest
set of packages that have been approved for inclusion in a future stable
environment.

!!! warning "Staging environments are subject to continuous change"
    The staging environments can and will change from day-to-day as
    more packages are approved by the SCCB, so no production analyses
    should rely upon them.

### igwn-testing {: #igwn-testing }

The [`igwn-testing`](./igwn-testing.md) environment includes the latest
packages that have been requested for inclusion in a future stable environment
via an [SCCB request](https://computing.docsligo.org/sccb/requests/).

!!! warning "Testing environments are subject to continuous change"
    The testing environments can and will change from day-to-day as
    more packages are approved by the SCCB, so no production analyses
    should rely upon them.

## Versioned environments {: #versioned }

The Default environments above track a specific _versioned_ environment
which is pinned to a minor release of Python (mainly for technical reasons).
As such the Default environments will periodically upgrade from one minor
Python version to the next to keep up-to-date.

To allow users to pin their workflow to a specific Python version, Versioned
environments are available that will always use the same version of Python:

- [`igwn-py39`](igwn-py39.md)
- [`igwn-py39-staging`](igwn-py39-staging.md)
- [`igwn-py39-testing`](igwn-py39-testing.md)
- [`igwn-py310`](igwn-py310.md)
- [`igwn-py310-staging`](igwn-py310-staging.md)
- [`igwn-py310-testing`](igwn-py310-testing.md)
- [`igwn-py311-testing`](igwn-py311-testing.md)

## Tagged environments {: #tagged }

Both the Default and Versioned environments described above will periodically
change to reflect requests to and approvals by the IGWN SCCB.
The IGWN Conda Distribution in CVMFS provides Tagged environments that
are fixed on creation and will never change.

The environments are named using the Python minor version and a date tag
reflecting when they were created, using the `YYYYMMDD.MICRO`
[CalVer](https://calver.org) versioning scheme.
For example an environment built with Python 3.9 on January 1st 2022 would
be named

```text
igwn-py39-20220101
```

!!! tip "Recommended for production analysis"
    Given their fixed contents, the tagged environments are recommended
    for production analysis that need to be reproducible.

## Supported Python versions {: #python-versions }

The IGWN Conda Distribution roughly follows the
[NEP 29](https://numpy.org/neps/nep-0029-deprecation_policy.html)
schedule for supporting minor Python versions, with the additional
requirement that each Python version is fully supported by conda-forge
(the relevant migration has been completed and that version is part of the
[conda-forge pinned packages](https://github.com/conda-forge/conda-forge-pinning-feedstock)).
This means that:

!!! example "IGWN Conda Distribution Python version support"

    IGWN Conda Distribution environments will be supported for all minor
    versions of (C)Python released within the 42 months prior to the
    Distribution release date that are also supported by conda-forge,
    and at minimum the two latest minor versions supported by conda-forge.

## Making changes {: #change-control }

See [_Change control_](../change-control.md) for details on how the package
lists are managed, and how to request or propose changes.
