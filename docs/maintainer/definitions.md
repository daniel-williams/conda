# Package definitions

This section describes how the package definitions are laid out inside the
[`computing/conda`](https://git.ligo.org/computing/conda) GitLab project.

## External packages {: #upstream }

External packages are defined as those not directly maintained by IGWN members
and over whose development we have little or no influence.
Examples include `gsl`, `numpy`, or `astropy`.

Pins for these packages are stored in
[`packages/upstream.yaml`](https://git.ligo.org/computing/conda/-/blob/staging/packages/upstream.yaml).

In general the following rules should be followed regarding packages in
`upstream.yaml`:

1.  A package should only be included if it is _explicitly_ required,
    meaning is it not a direct requirement of another package.
2.  A package should only be included if its version needs to be constrained
    in some way that is not specified in the requirements of another package.
3.  When a package is added/updated in `upstream.yaml`, include a good reason
    for the change (i.e. a link to an issue) __in the git commit message__.

Examples:

-   `numpy` is included because we wish to contrain its version to a specific
    minor release.

-   `notebook` is required because no other packages require it, but it is
    useful to allow users to spawn jupyter notebooks from the relevant conda
    environments.

-   `gsl` is __not__ included because it is automatically pulled in by its
    dependents, and we do not need to manually constrain its version.

## Internal packages {: #internal }

Internal packages are defined as those directly maintained by IGWN members
for the purposes of IGWN research.
Each of these packages is defined in its own file in the
[`packages/`](https://git.ligo.org/computing/conda/-/tree/staging/packages)
directory, named for the package.

Each package should be specified as precisely as possible, including the
appropriate `build_string`s for each Python version, platform, and
architecture to ensure compatibility with the rest of the distribution.

See [Workflow](./workflow.md) for more details on how to add or update
packages.
