# Package change workflow

This page attempts to describe the change workflow in order to implement
change requests that have been detailed on an SCCB ticket.

## Adding/updating packages {: #add }

### External {: #add-external }

To add/update an _External_ package, add or modify a single-line reference
to the package in the
[`packages/upstream.yaml`](https://git.ligo.org/computing/conda/-/blob/staging/packages/upstream.yaml).
file.

Please remember to follow the [guidance rules](index.md#upstream) regarding
external packages.

### Internal {: #add-internal }

To add/update an _Internal_ package, add or modify a YAML file for that
package.
This will typically require two things

1.   Identify the right `build_string` lines for each Python version,
     platform, and architecture.
1.   Work out what platforms, if any, need to be skipped.
1.   Configure some integration tests.

An in-place description of the YAML syntax can be found in the
[`packages/template.yaml`](https://git.ligo.org/computing/conda/-/blob/staging/packages/template.yaml)
exemplar.

#### Determining version numbers and build strings {: #build_string }

In order to add a new package, or upgrade an existing package, the new package
version and build string(s) need to be determined.

The version number should be declared in the SCCB ticket, which can then be used
to determine the build string by using the
[`search.py` script](https://git.ligo.org/computing/conda/-/blob/staging/scripts/search.py)
(from _this_ repo):

```console
python ./scripts/search.py <package> <version>
```

!!! example "search.py output"
    ```console
    > python ./scripts/search.py python-framel 8.40.1
    ```
    This will print something like this:

    ```yaml
    package:
      name: python-framel
      version: 8.40.1
      build_string: py36h785e9b2_0  # [linux and x86_64 and py==36]
      build_string: py37h03ebfcd_0  # [linux and x86_64 and py==37]
      build_string: py38h8790de6_0  # [linux and x86_64 and py==38]
      build_string: py36h255dfe6_0  # [osx and x86_64 and py==36]
      build_string: py37h10e2902_0  # [osx and x86_64 and py==37]
      build_string: py38h65ad66c_0  # [osx and x86_64 and py==38]
      build_string: py36h9902d54_0  # [win and x86_64 and py==36]
      build_string: py37h9dff50a_0  # [win and x86_64 and py==37]
      build_string: py38h377fac3_0  # [win and x86_64 and py==38]
    ```

!!! tip "Support multiple platforms using selectors"
    Specifying a unique `build_string` for multiple platforms, architectures,
    and Python versions is achieved using conda-build's pre-processing
    [selectors](https://docs.conda.io/projects/conda-build/en/latest/resources/define-metadata.html#preprocessing-selectors).

    These selectors can be used on any line of the YAML file, and only
    when the selector evaluates to `true` will that line actually be used
    to define a package.

!!! tip "Restricting the build"
    You can use the `--build` option to specify a build string (or wildcard)
    to restrict the results to a single build.

#### Skipping a package on certain platforms {: #skip }

While we strive to provide the IGWN Conda Distribution on a wider variety of
platforms and architectures, not all packages are built for all platforms,
architectures, or Python versions,
so it may be necessary to declare `skip: true` for some packages.
This should be done as a top-level key in the YAML file using
[selectors](https://docs.conda.io/projects/conda-build/en/latest/resources/define-metadata.html#preprocessing-selectors)
to restrict when a package is skipped.

!!! example "Common skip examples"

    === "Skip on Python < 3.9"

        ```yaml
        skip: true  # [py<39]
        ```

    === "Skip on Windows and non-x86_64 Linux"

        ```yaml
        skip: true  # [win or (linux and not x86_64)]
        ```

#### Optional requirements {: #optional }

An internal package should declare its own optional requirements as a `list`
specified by the `requirements` key:

!!! example "Optional requirements"

    ```yaml
    requirements:
      - pytest-freezegun
    ```

Use cases for optional requirements include (but are not limited to):

-   supporting an optional feature
-   test requirements that aren't runtime requirements
-   constraining a third-party package that is known to conflict with the
    internal package

#### Integration tests {: #tests }

Each internal package should be tested to ensure its integration
into the distribution
In most cases this will mean adding tests directly to the YAML file for
that package, however in __some cases__ a package is best tested by
running tests defined for a downstream package.

There are a number of different types of tests, all of which can be used
on their own, or together for a package.

##### Import tests {: #test-import }

`import` tests specify one or more Python modules that are imported in a
Python session as a basic sanity check.

Example:

```yaml
test:
  imports:
    - package
    - package.module
```

##### `pytest` tests {: #test-pytest }

`pytest` tests declare a script or module that should be executed using
[pytest](https://docs.pytest.org/).

For a custom Python script, the argument for the `test/pytest` option
should just be the name of the script, which should them be added to the
`computing/conda` project alongside the YAML file:

```yaml
test:
  pytest: mypackage-tests.py
```

For packages that bundle their own test suite as a subpackage that is
installed, you can declare the test using `--pyargs`:

```yaml
test:
  pytest: --pyargs mypackage
```

The `test/pytest` option can also specify a list of independent test
scripts/modules to execute:

```yaml
test:
  pytest:
    - mypackage-tests.py
    - --pyargs mypackage
```

!!! note "`pytest` command execution"

    `pytest` tests are executed using the following invocation:

    ```shell
    python -m pytest --cache-clear --no-header -r a <args>
    ```

    where `<args>` is the literal content of the `test/pytest` entry.

##### Script tests {: #test-scripts }

`script` tests specify a Python, Bash, or Powershell (dependening on the
platform) script file that is executed to test a package.

Example:

```yaml
test:
  script: mything-tests.sh
```

Each `script` should be added to the `computing/conda` project alongside the
YAML file in the
[`packages/`](https://git.ligo.org/computing/conda/-/tree/staging/packages/).
directory.

!!! note "Script execution"

    `script` tests are executed using the following invocation based on
    the file extension of the script option:

    === "Bash (Unix)"

        ```shell
        bash -e <args>
        ```

    === "Bash (Windows)"

        ```shell
        bash.exe -el <args>
        ```

    === "Perl"

        ```shell
        perl <args>
        ```

    === "Python"

        ```shell
        python <args>
        ```

    All other file extensions will result in an error, however it should
    be straightforward to modify
    [`scripts/render.py`](https://git.ligo.org/computing/conda/-/blob/staging/scripts/render.py)
    to support new extensions.

!!! note "Tests run in unique, temporary directories"

    Each `script` is copied at runtime into a unique temporary directory, so
    must be able to run independently of the contents of the `computing/conda`
    project.
    The temporary directory is automatically cleaned up once the test
    completes, so manual clean up of created/downloaded files is not required.

##### Command tests {: #test-commands }

`command` tests specify lines of shell code that are executed to test a
package.

For example, to run a single command:

```yaml
test:
  command: mything --help
```

or to run multiple commands, specify a list:

```yaml
test:
  commands:
    - mything --help
    - myotherthing --help
```

Each line is treated as an indepdendent test that is executed literally in a
unique process, so multiple lines cannot be chained to form a script.
In that case use a [Script test](#test-scripts).

## Getting packages into testing {: #testing }

To get packages in testing,
[open a merge request](https://git.ligo.org/computing/conda/merge_requests/new?merge_request%5Btarget_branch%5D=testing) against `testing`.
The CI will run to validate things.

Once the CI passes, **the MR can be merged immediately** in order to deploy the
new packages into the testing environments.

## Migrating packages from testing to staging {: #staging }

To migrate packages from `testing` into `staging`:

1. go back to the merge request you posted for `testing`,
   and click the _Cherry-pick_ button,
2. in the popup, under _Pick into branch_, select `staging`
3. ensure that the _Start a new merge request with these changes_ radio box
   is **checked**
4. Click _Cherry-pick_.

!!! tip "Apply the right labels"
    Please then apply the _sccb::pending_ label to indicate that the SCCB
    is still considering this request.
    Please also apply any other labels that seem appropriate.

!!! warning "Wait for SCCB approval"
    Once the CI passes, **wait until the SCCB approves the request**.

As soon as the SCCB approves the request, the MR should then be merged to
deploy the new packages into the staging environments.

## Creating a new stable release {: #release }

When deployment time arrives, a new stable distribution should be created,
see [_Tagging a release_](./release.md).

## Troubleshooting

### Determining why a package isn't available {: #debug-missing }

If `search.py` errors, it may be because a package (at a particular version)
isn't available in `conda-forge`.
You can perform a basic search to see if _any_ version of that package
is available (useful if the requested package is new):

```bash
conda search my-new-package
```

From here there are three likely outcomes:

1. The package isn't available at any version; it is likely that the requested
   package simply isn't packaged for conda yet.
   In that case you can check the conda-forge
   [`staged-recipes`](https://github.com/conda-forge/staged-recipes/)
   repository's
   [Pull Requests list](https://github.com/conda-forge/staged-recipes/pulls)
   to see if there's a PR marked _Add my-new-package_.

       If there _is_ a PR, just keep track of it and add the package when it
       becomes available.
       If there _isn't_ a PR, you should contact the developer to ask whether they
       plan to add one, if they don't have a plan, give them a stern look and
       ask them to make one.

2. The package isn't available at the requested version, only earlier ones;
   it is likely that the conda-forge feedstock hasn't yet been updated to
   reflect the new upstream release.
   In this case you can check the feedstock repo Pull Requests list at

       https://github.com/conda-forge/my-new-package-feedstock/pulls/

       (replace `my-new-package` with an actual package name). If the feedstock
       is well formed the conda-forge autotick bot should make a PR automatically
       when an upstream tarball is uploaded (e.g. to pypi.python.org), otherwise
       you might have to open a ticket on the feedstock, or just directly contact
       the feedstock maintainers (likely the developers of the package themselves)
       to see what's going on.

3. The package isn't available at the specifically requested version, but is
   available at newer versions. In this case it's more trouble than it's worth
   to attempt to backport the given version for conda-forge, you should go back
   to the requester and see if the closest matching version that does exist in
   conda-forge will fulfil their request.

### Resolving conflicts {: #debug-conflicts }

It is relatively common, when updating a package, that the new/updated package
conflicts with one or more existing packages in the distribution.
In my experience there are two reasons for this:

1.  __Direct conflict__, where the requirements for the new/updated package
    and one of the existing packages form a mutually exclusive set.
    In this case the only thing you can really do is go back to the
    requester (of the new/updated package) and tell them there's a conflict.

    You may be able to convince the maintainer of the conflicting packages
    to update their requirements to resolve things.

2.  __Migration-based conflict__, where the new/updated package has been built
    against a new version of some upstream library while existing packages
    were built against an older version.

    This is typically the case when conda-forge has
    [migrated](https://conda-forge.org/blog/posts/2019-12-06-cfep09/) the
    upstream package to a new ABI version.

    In this case all immediate dependents of the relevant migrated upstream
    package must be updated to a new build that was made against the new
    upstream package.
    Examples of upstream packages for which migrations may require this
    include `gsl` and `root_base`.
    The full list of pinned packages that are subject to migration rules
    are found in the
    [conda-forge-pinning-feedstock](https://github.com/conda-forge/conda-forge-pinning-feedstock/blob/main/recipe/conda_build_config.yaml).
