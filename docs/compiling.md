# Compiling code in a Conda environment

!!! tip "Use the pre-built environments"

    The [Pre-built environments](/environments/) should contain all of the
    packages needed to build most things, if packages are missing please
    [open a Request ticket](https://git.ligo.org/sccb/requests/new)
    to request their addition.

## Installing a compiler

To compile packages into a conda environment, it is recommended to install
all of the build tools into that environment, including the necessary
compiler toolchain.
With conda-forge, this is simplified by a series of metapackages that
can be used to install the right compiler for the given language:

| Language | Package name |
| -------- | ------------ |
| C | `c-compiler` |
| C++ | `cxx-compiler` |
| Fortran | `fortran-compiler` |

!!! example "Example: installing the C++ compiler package"
    ```bash
    conda install cxx-compiler
    ```

    On Linux the above command will install the GNU C++ compiler (`g++`),
    while on macOS this will install Clang.

## Installation prefices

Conda's environment configuration means that dynamic linking is much more
tightly controlled than typically done on other platforms.
This means that, for example, building shared object libraries and
installing them into custom directories that you append to environment
variables such as `LD_LIBRARY_PATH` may not resolve links properly.

The way to avoid this is simply to always install code directly into a
conda environment.

=== "Autotools"

    With `autotools` you should use

    ```shell
    ./configure --prefix=${CONDA_PREFIX} ...
    ```

=== "CMake"

    With `CMake` the conda-forge compilers provide an environment variable
    `${CMAKE_ARGS}` that includes the basics needed to configure CMake
    properly:

    ```shell
    cmake . ${CMAKE_ARGS} ...
    ```

=== "Meson"

    With `meson` the conda-forge compilers provide an environment variable
    `${MESON_ARGS}` that includes the basics needed to configure meson
    properly:

    ```shell
    meson setup . ${MESON_ARGS} ...
    ```

=== "Python"

    With Python (or Pip) you shouldn't need to manually specify any paths,
    just make sure you are using the right Python installation:

    ```shell
    python -m pip install .
    ```

## Installing the macOS SDK {: #sdk }

Conda compilers will try and compile software for the oldest compatible
version of macOS, which is 10.9 most of the time.
So, you will likely need to install the MacOSX10.9 SDK, which you can do
as follows:

```shell
curl -L -O https://github.com/phracker/MacOSX-SDKs/releases/download/11.3/MacOSX10.9.sdk.tar.xz
mkdir -p /opt
tar -xf MacOSX10.9.sdk.tar.xz -C /opt
```

!!! note "Using `/opt` is just a convention"
    You can store the SDK wherever you like, using `/opt` is just
    a convention.

Then, set the `CONDA_BUILD_SYSROOT` environment variable to match the
new SDK:

```shell
export CONDA_BUILD_SYSROOT="/opt/MacOSX10.9.sdk"
```

If you indend to use `conda-build` to test package builds, you should also
add the SDK path to your global `~/conda_build_config.yaml` file:

```yaml
CONDA_BUILD_SYSROOT:     # [osx]
  - /opt/MacOSX10.9.sdk  # [osx]
```
