---

include:
  - project: computing/gitlab-ci-templates
    file:
      - conda.yml
      - debian.yml
      - python.yml

# -- configuration ------------------------------

default:
  interruptible: true

stages:
  - render
  - test
  - build
  - docs
  - rsync
  - deploy
  - release

variables:
  CPU_COUNT: 4
  GIT_STRATEGY: "clone"
  # cvmfs info
  CVMFS_REPO: "software.igwn.org"
  CVMFS_PATH: "/cvmfs/${CVMFS_REPO}/conda"
  CVMFS_USER: "cvmfs.conda"
  CVMFS_REPO_HOST: "cvmfs-software.ligo.caltech.edu"
  RSYNC_OPTS: "--archive --itemize-changes --rsh=ssh --log-file=$CI_PROJECT_DIR/rsync.log"

# -- code snippets ------------------------------

.retry: &define-retry |
  retry() {
    local n=1
    local max=3
    local delay=30
    while true; do
      "$@" && break || {
        exit_code="$?"
        if [[ $n -lt $max ]]; then
          echo -e "\x1B[93mCommand failed (attempt $n/$max), retrying in $delay seconds...\x1B[0m" 1>&2;
          ((n++))
          sleep $delay;
        else
          echo -e "\x1B[91mThe command has failed after $n attempts.\x1B[0m" 1>&2;
          exit ${exit_code};
        fi
      }
    done
  }

.docker_tag: &define-docker-tag
  - if [ -z ${CI_COMMIT_TAG} ]; then
        DOCKER_TAG="${CI_REGISTRY_IMAGE}:${CI_COMMIT_BRANCH}";
    else
        DOCKER_TAG="${CI_REGISTRY_IMAGE}:${CI_COMMIT_TAG}";
    fi
  - export DOCKER_TAG

.configure_cvmfs_ssh: &configure-cvmfs-ssh
  - apt-get -y -q install coreutils openssh-client
  - eval $(ssh-agent -s)
  - mkdir -p ~/.ssh
  - ssh-add <(echo "${CVMFS_SSH_PRIVATE_KEY}" | base64 -d)
  - if [[ "${CI_RUNNER_TAGS}" == *"cit"* ]]; then export CVMFS_REPO_HOST="${CVMFS_REPO_HOST/ligo.caltech.edu/ldas.cit}"; fi
  - echo -e "Host ${CVMFS_REPO_HOST}\n\tStrictHostKeyChecking no\n" >> ~/.ssh/config

# -- job rules ----------------------------------

.tag-rule: &if-stable
  - if: '$CI_PROJECT_PATH == "computing/conda" && $CI_COMMIT_TAG'

.staging-rule: &if-staging
  - if: '$CI_PROJECT_PATH == "computing/conda" && $CI_COMMIT_BRANCH == "staging"'

.staging-mr-rule: &if-staging-mr
  - if: '$CI_PIPELINE_SOURCE == "merge_request_event" && $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "staging"'

.testing-rule: &if-testing
  - if: '$CI_PROJECT_PATH == "computing/conda" && $CI_COMMIT_BRANCH == "testing"'

.testing-mr-rule: &if-testing-mr
  - if: '$CI_PIPELINE_SOURCE == "merge_request_event" && $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "testing"'

.skip-deploy-rule: &if-not-skip-deploy
  - if: '$CI_COMMIT_MESSAGE =~ /\[skip deploy\]/'
    when: never

.need-docs-rule: &if-docs
  - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
    changes:
      - scripts/write_env_md.py
      - docs/**/*
      - mkdocs.yml
    variables:
      # --no-directory-urls: direct URLs make artifacts easier to navigate
      # --strict: fail on warnings
      # --verbose: print more stuff
      MKDOCS_OPTIONS: "--no-directory-urls --strict --verbose"
  - if: '$CI_PIPELINE_SOURCE == "merge_request_event" && $CI_COMMIT_MESSAGE =~ /\[build docs\]/'
    variables:
      MKDOCS_OPTIONS: "--no-directory-urls --strict --verbose"

.trigger-test-docker-rule: &if-test-docker
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      changes:
        - igwn_conda_config.yaml
        - scripts/build.py
        - scripts/build-cvmfs.dockerfile

# -- job templates ------------------------------

.cache:
  variables:
    # top-level cache directory
    CACHE_DIR: "${CI_PROJECT_DIR}/.cache"
    XDG_CACHE_HOME: "${CACHE_DIR}"
    # application-specific caches
    APT_CACHE_DIR: "${CACHE_DIR}/apt"
    CONDA_PKGS_DIRS: "${CACHE_DIR}/conda/pkgs"
  after_script:
    # clean out packages that weren't opened during this run
    # (most likely older versions that have been superseded)
    - find ${APT_CACHE_DIR}/archives -type f -maxdepth 1 -atime +1 -delete
    - find ${CONDA_PKGS_DIRS} -type f -maxdepth 1 -atime +1 -delete
  cache:
    key: "${CI_JOB_NAME}"
    paths:
      # things cached via astropy
      - .cache/*/download
      # debian packages
      - .cache/apt/archives/*.deb
      # conda packages
      - .cache/conda/pkgs/*.conda
      - .cache/conda/pkgs/*.tar.bz2
      # misc application caches
      - .cache/gpstime
      - .cache/psrqpy

.debian:
  extends:
    - .debian:base
  image: debian:stable

.conda:
  extends:
    # pick up variables from templates
    - .debian
    - .conda:base
    # use the same cache for everything
    - .cache
  image: igwn/base:conda
  before_script:
    - *define-retry
    # init apt
    - !reference [".debian:base", before_script]
    # init conda
    - !reference [".conda:base", before_script]
    - conda config --file ${CONDARC} --append channels igwn
    - conda update -n base conda conda-libmamba-solver
    - conda config --file ${CONDARC} --set solver libmamba
    # install a conda version that doesn't break virtual packages,
    # see https://github.com/conda/conda/issues/12219
    - if [ "${SUBDIR}" != "linux-64" ]; then
      retry conda install --solver libmamba --quiet --yes --name base --satisfied-skip-solve "conda<22.11.0|>=23.3.0";
      fi
    # print packages in the base environment
    - conda list --name base

.linux:
  extends: .conda

.linux-64:
  extends: .linux
  variables:
    SUBDIR: "linux-64"

.linux-aarch64:
  extends: .linux
  variables:
    SUBDIR: "linux-aarch64"

.linux-ppc64le:
  extends: .linux
  variables:
    SUBDIR: "linux-ppc64le"

.osx:
  extends: .conda
  variables:
    # set fake osx version for __osx virtual package
    CONDA_OVERRIDE_OSX: "14.0"

.osx-64:
  extends: .osx
  variables:
    SUBDIR: "osx-64"

.osx-arm64:
  extends: .osx
  variables:
    SUBDIR: "osx-arm64"

.win:
  extends: .conda

.win-64:
  extends: .win
  variables:
    SUBDIR: "win-64"

# -- render -------------------------------------

.render:
  stage: render
  variables:
    # we need all branches
    GIT_DEPTH: 0
    GIT_STRATEGY: "clone"
  script:
    - *define-retry
    # install requirements
    - retry conda create --name render --quiet --yes --file requirements.txt
    - conda activate render
    - conda list --name render
    # clean the index cache to make sure we get a clean solve
    - conda clean --index-cache --yes
    # set platform and check config
    - export CONDA_SUBDIR="${SUBDIR}"
    - conda info
    # render
    - xargs -t python ./scripts/render.py
          packages
          --config-file ./igwn_conda_config.yaml
          --output-dir "environments/${SUBDIR}"
          --verbose
          <<< ${RENDER_OPTIONS}
  artifacts:
    paths:
      - environments/
    expire_in: 1 day
  retry:
    max: 2
    when:
      - runner_system_failure

.render:stable:
  extends:
    - .render
  rules:
    # all production builds
    - *if-stable
    - *if-staging
    - *if-testing
    # and if we need to test the docs or docker
    - *if-docs
    - *if-test-docker
  variables:
    RENDER_OPTIONS: "--skip-staging --skip-testing"

.render:staging:
  extends:
    - .render
  rules:
    # all production builds
    - *if-stable
    - *if-staging
    - *if-testing
    # and MRs against production
    - *if-staging-mr
    # and if we need to test the docs or docker
    - *if-docs
    - *if-test-docker
  variables:
    RENDER_OPTIONS: "--skip-stable --skip-testing"

.render:testing:
  extends:
    - .render
  rules:
    # all production builds
    - *if-stable
    - *if-staging
    - *if-testing
    # and MRs against testing
    - *if-testing-mr
    # and if we need to test the docs or docker
    - *if-docs
    - *if-test-docker
  variables:
    RENDER_OPTIONS: "--skip-stable --skip-staging"

render:linux-64:stable:
  extends:
    - .linux-64
    - .render:stable

render:linux-64:staging:
  extends:
    - .linux-64
    - .render:staging

render:linux-64:testing:
  extends:
    - .linux-64
    - .render:testing

render:linux-aarch64:stable:
  extends:
    - .linux-aarch64
    - .render:stable

render:linux-aarch64:staging:
  extends:
    - .linux-aarch64
    - .render:staging

render:linux-aarch64:testing:
  extends:
    - .linux-aarch64
    - .render:testing

render:linux-ppc64le:stable:
  extends:
    - .linux-ppc64le
    - .render:stable

render:linux-ppc64le:staging:
  extends:
    - .linux-ppc64le
    - .render:staging

render:linux-ppc64le:testing:
  extends:
    - .linux-ppc64le
    - .render:testing

render:osx-64:stable:
  extends:
    - .osx-64
    - .render:stable

render:osx-64:staging:
  extends:
    - .osx-64
    - .render:staging

render:osx-64:testing:
  extends:
    - .osx-64
    - .render:testing

render:osx-arm64:stable:
  extends:
    - .osx-arm64
    - .render:stable

render:osx-arm64:staging:
  extends:
    - .osx-arm64
    - .render:staging

render:osx-arm64:testing:
  extends:
    - .osx-arm64
    - .render:testing

render:win-64:stable:
  extends:
    - .win-64
    - .render:stable

render:win-64:staging:
  extends:
    - .win-64
    - .render:staging

render:win-64:testing:
  extends:
    - .win-64
    - .render:testing

# -- test ---------------------------------------

.test:
  stage: test
  tags:
    # use UWM only because it has a big fat node with lots of
    # disk space
    - uwm
  variables:
    # configure standard tool cache and config directories
    XDG_CONFIG_HOME: "${CI_PROJECT_DIR}/.config"
    # configure condor to use a configuration file, not /dev/null
    CONDOR_CONFIG: "${XDG_CONFIG_HOME}/condor_config"
    # cache data from GWpy
    GWPY_CACHE: 1
    # force individual tests to only use one core
    # (list taken from
    #  https://github.com/htcondor/htcondor/blob/v23.0.0/src/condor_utils/param_info.in#L2693)
    CUBACORES: 1
    GOMAXPROCS: 1
    JULIA_NUM_THREADS: 1
    MKL_NUM_THREADS: 1
    NUMEXPR_NUM_THREADS: 1
    OMP_NUM_THREADS: 1
    OMP_THREAD_LIMIT: 1
    OPENBLAS_NUM_THREADS: 1
    TF_LOOP_PARALLEL_ITERATIONS: 1
    TF_NUM_THREADS: 1
    # set variables to help the tests
    USER: "igwn-conda"
  script:
    # if running on a CIT highmem runner, use more cores
    - if [[ "${CI_RUNNER_TAGS}" == *"cit"* ]] && [[ "${CI_RUNNER_TAGS}" == *"highmem"* ]]; then
          export CPU_COUNT=11;
      fi
    # parse job name to environment name
    - "ENV_NAME=${CI_JOB_NAME##*:}"
    - ENV_DIR="environments/${SUBDIR}/${ENV_NAME}"
    # install extra system requirements on Linux
    - if [[ "${SUBDIR}" =~ ^linux.* ]]; then
          xargs -a <(awk '! /^ *(#|$)/' apt_requirements.txt) -r -- apt-get -y -q install;
      fi
    # init XDG cache directories
    - mkdir -pv
          ${XDG_CACHE_HOME}/astropy
          ${XDG_CACHE_HOME}/gwpy
          ${XDG_CACHE_HOME}/ligo.em_bright
    # create empty condor config file,
    # otherwise condor uses /dev/null, which gets used by other processes
    # and things get a bit confused (I think)
    - if [ ! -z "${CONDOR_CONFIG}" ]; then
          mkdir -p $(dirname ${CONDOR_CONFIG});
          touch "${CONDOR_CONFIG}";
      fi
    # create environment
    - *define-retry
    - retry conda env create
          --file ${ENV_DIR}/${ENV_NAME}.yaml
          --force
          --quiet
          --solver libmamba
    - conda activate ${ENV_NAME}
    # print packages (helps debugging)
    - conda list
    # get authentication token (if available)
    - |
      if [[ ! -z "${ROBOT_LIGO_KEYTAB}" ]]; then
          bash -e scripts/auth.sh;
      else
          echo -e "\x1B[33mNo robot Kerberos credential found, integration testing will be incomplete (more than usual).\x1B[0m";
      fi
    # run tests
    - cd ${ENV_DIR}
    - python -m pytest
          ${ENV_NAME}-test-*.py
          -r a
          --durations 0
          --junit-xml "${CI_PROJECT_DIR}/${ENV_NAME}-test-report.xml"
          --numprocesses ${CPU_COUNT}
          --reruns 1
          --timeout 1800
  artifacts:
    reports:
      junit: "*-test-report.xml"
  retry:
    max: 2
    when:
      - job_execution_timeout
      - runner_system_failure
      - stuck_or_timeout_failure

.test-linux-64:
  extends:
    - .linux-64
    - .test

.test-stable:
  rules:
    # only test stable environments on production builds
    - *if-stable

.test-staging:
  rules:
    # only test on production staging builds or MRs against staging
    - *if-staging
    - *if-staging-mr

.test-testing:
  rules:
    # only test on production testing builds or MRs against testing
    - *if-testing
    - *if-testing-mr

# --

test:linux-64:igwn-py39:
  extends:
    - .test-linux-64
    - .test-stable
  needs:
    - render:linux-64:stable

test:linux-64:igwn-py39-staging:
  extends:
    - .test-linux-64
    - .test-staging
  needs:
    - render:linux-64:staging

test:linux-64:igwn-py39-testing:
  extends:
    - .test-linux-64
    - .test-testing
  needs:
    - render:linux-64:testing

test:linux-64:igwn-py310:
  extends:
    - .test-linux-64
    - .test-stable
  needs:
    - render:linux-64:stable

test:linux-64:igwn-py310-staging:
  extends:
    - .test-linux-64
    - .test-staging
  needs:
    - render:linux-64:staging

test:linux-64:igwn-py310-testing:
  extends:
    - .test-linux-64
    - .test-testing
  needs:
    - render:linux-64:testing

test:linux-64:igwn-py311-testing:
  extends:
    - .test-linux-64
    - .test-testing
  needs:
    - render:linux-64:testing

test:docker:
  stage: test
  image: docker:latest
  tags:
    - executor-docker
  rules:
    # only if the docker configuration has changed
    - *if-test-docker
  needs:
    - render:linux-64:stable
    - render:linux-64:staging
    - render:linux-64:testing
  script:
    # build out the environments using docker build
    - docker build .
          --file "scripts/build-cvmfs.dockerfile"
          --pull
          --no-cache
          --build-arg "INSTALL_PATH=${CVMFS_PATH}"
          ${BUILD_OPTS}

# -- CVMFS build ------------

.build:
  stage: build
  image: docker:latest
  tags:
    - executor-docker
    # use UWM only because it has a big fat node with lots of
    # disk space
    - uwm
  needs:
    - render:linux-64:stable
    - render:linux-64:staging
    - render:linux-64:testing
  script:
    - docker login -u gitlab-ci-token -p ${CI_JOB_TOKEN} ${CI_REGISTRY}
    - *define-docker-tag
    # build out the environments using docker build
    - docker build .
          --file "scripts/build-cvmfs.dockerfile"
          --pull
          --no-cache
          --tag "${DOCKER_TAG}"
          --build-arg "INSTALL_PATH=${CVMFS_PATH}"
          --build-arg "BUILD_OPTS=${BUILD_OPTS}"
    - docker push "${DOCKER_TAG}"
  retry: 2

build:stable:
  extends:
    - .build
  rules:
    # only for stable production builds
    - *if-stable
  needs:
    - render:linux-64:stable
  before_script:
    - BUILD_OPTS="--tag ${CI_COMMIT_TAG} --skip-staging --skip-testing"

build:staging:
  extends:
    - .build
  rules:
    # only for production staging builds
    - *if-staging
  needs:
    - render:linux-64:staging
  before_script:
    - BUILD_OPTS="--skip-testing"

build:testing:
  extends:
    - .build
  rules:
    # only for production testing builds
    - *if-testing
  needs:
    - render:linux-64:testing
  before_script:
    - BUILD_OPTS="--skip-staging"

# -- CVMFS rsync ------------

.cvmfs_server:
  # start a new publish operation
  transaction:
    - |
      ssh ${CVMFS_USER}@${CVMFS_REPO_HOST} <<EOF
      # this sets RW permissions on the CVMFS_REPO path and locks it;
      sudo -u repo.software cvmfs_server transaction -t 300 ${CVMFS_REPO}
      # force permissions on the repo base directory again
      # see https://git.ligo.org/computing/helpdesk/-/issues/4051#note_726376
      sudo -u repo.software chmod go+rx "/cvmfs/${CVMFS_REPO}"
      EOF

  # finish the publish operation
  publish:
    # this completes the transation, sets RO permissions, and unlocks
    # (fall back to 'abort' if that fails)
    - |
      ssh ${CVMFS_USER}@${CVMFS_REPO_HOST} <<EOF
      sudo -u repo.software cvmfs_server publish ${CVMFS_REPO} || \
      { sudo -u repo.software cvmfs_server abort -f ${CVMFS_REPO}; exit 1; }
      EOF

  # abort the publish operation
  abort:
    - ssh ${CVMFS_USER}@${CVMFS_REPO_HOST}
      sudo -u repo.software cvmfs_server abort -f ${CVMFS_REPO}

  # handle which operation based on the exit code
  publish-or-abort:
    # if the rsync command wrote an exit-code of 0 then 'publish', otherwise 'abort'
    - if [ "${exit_code:=$?}" -eq 0 ]; then
        cvmfsop="publish";
        echo -e "\x1B[92mRsync succeeded, executing cvmfs_server ${cvmfsop}\x1B[0m";
      else
        cvmfsop="abort -f";
        echo -e "\x1B[91mRsync failed, executing cvmfs_server ${cvmfsop}\x1B[0m";
      fi
    - |
      ssh ${CVMFS_USER}@${CVMFS_REPO_HOST} << EOF
      sudo -u repo.software cvmfs_server ${cvmfsop} ${CVMFS_REPO} || \
      { sudo -u repo.software cvmfs_server abort -f ${CVMFS_REPO}; exit 1; }
      EOF
    # propagate the exit code
    - exit ${exit_code}

.rsync-cvmfs:
  extends:
    - .debian
  stage: rsync
  tags:
    # we need to use docker
    - executor-docker
  interruptible: false
  variables:
    GIT_STRATEGY: none
  before_script:
    - !reference [".debian:base", before_script]
    # install software
    - *configure-cvmfs-ssh
    - apt-get -y -q install
          docker.io
          gzip
          rsync
          tar
    # authenticate with local container registry
    - docker login -u gitlab-ci-token -p ${CI_JOB_TOKEN} ${CI_REGISTRY}
    - *define-docker-tag
    # create, then export the container to unpack the environments
    - mkdir -p _build
    - DOCKER_NAME="tmp${RANDOM}"
    - docker pull "${DOCKER_TAG}"
    - docker create --name "${DOCKER_NAME}" "${DOCKER_TAG}" /bin/python
    - docker export ${DOCKER_NAME} | tar -x -C _build
    - docker rm --force "${DOCKER_NAME}"
    - docker rmi "${DOCKER_TAG}"
    # sanity check things locally first
    - head -n 1 ./_build/condabin/conda
    - ./_build/bin/python ./_build/condabin/conda info --all
  artifacts:
    paths:
      - rsync.log
    when: always

rsync:stable:
  extends:
    - .rsync-cvmfs
  rules:
    # only for stable production builds where deployment hasn't been
    # manually skipped
    - *if-not-skip-deploy
    - *if-stable
  resource_group: cvmfs-rsync
  environment:
    name: stable
    deployment_tier: production
  script:
    # start a new transaction
    - !reference [".cvmfs_server", "transaction"]
    # sync the base conda environment
    - echo "${RSYNC_OPTS}"
    - rsync
          ${RSYNC_OPTS}
          --filter='Pp .cvmfscatalog' --filter='Pp .cvmfsautocatalog'
          --delete
          --exclude envs
          ./_build/
          ${CVMFS_USER}@${CVMFS_REPO_HOST}:${CVMFS_PATH}/
      1>/dev/null || exit_code=$?
    # sync the stable environments (only if the base sync worked)
    - if [ "${exit_code:-0}" -eq 0 ]; then
      rsync
          ${RSYNC_OPTS}
          --filter='Pp .cvmfscatalog' --filter='Pp .cvmfsautocatalog'
          ./_build/envs/
          ${CVMFS_USER}@${CVMFS_REPO_HOST}:${CVMFS_PATH}/envs/
      1>/dev/null || exit_code=$?; fi
    # publish the new files, or abort
    - !reference [".cvmfs_server", "publish-or-abort"]

rsync:staging:
  extends:
    - .rsync-cvmfs
  rules:
    # only for production staging builds where deployment hasn't been
    # manually skipped
    - *if-not-skip-deploy
    - *if-staging
  resource_group: cvmfs-rsync
  environment:
    name: staging
    deployment_tier: staging
  script:
    # start a new transaction
    - !reference [".cvmfs_server", "transaction"]
    # sync the staging environments
    - echo "${RSYNC_OPTS}"
    - rsync
          ${RSYNC_OPTS}
          --filter='Pp .cvmfscatalog' --filter='Pp .cvmfsautocatalog'
          --delete
          ./_build/envs/*-staging
          ${CVMFS_USER}@${CVMFS_REPO_HOST}:${CVMFS_PATH}/envs/
      1>/dev/null || exit_code=$?
    # publish the new files, or abort
    - !reference [".cvmfs_server", "publish-or-abort"]

rsync:testing:
  extends:
    - .rsync-cvmfs
  rules:
    # only for production testing builds where deployment hasn't been
    # manually skipped
    - *if-not-skip-deploy
    - *if-testing
  resource_group: cvmfs-rsync
  environment:
    name: testing
    deployment_tier: testing
  script:
    # start a new transaction
    - !reference [".cvmfs_server", "transaction"]
    # sync the testing environments
    - echo "${RSYNC_OPTS}"
    - rsync
          ${RSYNC_OPTS}
          --filter='Pp .cvmfscatalog' --filter='Pp .cvmfsautocatalog'
          --delete
          ./_build/envs/*-testing
          ${CVMFS_USER}@${CVMFS_REPO_HOST}:${CVMFS_PATH}/envs/
      1>/dev/null || exit_code=$?
    # publish the new files, or abort
    - !reference [".cvmfs_server", "publish-or-abort"]

# -- documentation ----------

docs:
  extends:
    # https://computing.docs.ligo.org/gitlab-ci-templates/python/#.python:mkdocs
    - .python:mkdocs
  stage: docs
  image: igwn/base:conda
  variables:
    REQUIREMENTS: "-r docs/requirements.txt"
  rules:
    # all production builds
    - *if-stable
    - *if-staging
    - *if-testing
    # or if the docs configuration/content has been modified
    - *if-docs
  needs:
    - render:linux-64:stable
    - render:linux-64:staging
    - render:linux-64:testing
    - render:osx-64:stable
    - render:osx-64:staging
    - render:osx-64:testing
    - render:osx-arm64:stable
    - render:osx-arm64:staging
    - render:osx-arm64:testing
    - render:win-64:stable
    - render:win-64:staging
    - render:win-64:testing
  script:
    # convert environments
    - ${PYTHON} -u scripts/write_env_md.py
    # copy environment YAML files into output HTML paths
    - SUBDIRS=$(PYTHONPATH=scripts python3 -c "from write_env_md import find_subdirs; print(' '.join(find_subdirs()))")
    - |
      for SUBDIR in ${SUBDIRS}; do
          _TARGET="docs/environments/${SUBDIR}/";
          mkdir -p ${_TARGET};
          cp -v environments/${SUBDIR}/*/*.yaml ${_TARGET};
      done
    # provide legacy symlinks for -64 platforms
    - for SUBDIR in linux osx win; do
          (cd docs/environments; ln -s -v ${SUBDIR}-64 ${SUBDIR});
      done
    # render docs
    - !reference [".python:mkdocs", script]

pages:
  stage: deploy
  rules:
    # all production builds
    - *if-stable
    - *if-staging
    - *if-testing
  needs:
    - docs
  variables:
    GIT_STRATEGY: "none"
  script:
    - mv site public
  artifacts:
    paths:
      - public

# -- release ----------------

release-notes:
  extends:
    - .conda:base
    - .cache
  image: igwn/base:conda
  stage: docs
  rules:
    # only for production stable/staging builds
    - *if-stable
    - *if-staging
    # or MRs proposed for staging
    - *if-staging-mr
    # or if the relevant scripts have been modified
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      changes:
        - scripts/diff.py
        - scripts/release-description.sh
  needs: []
  variables:
    GIT_DEPTH: 0
    GIT_STRATEGY: "clone"
  script:
    - conda create --name release --solver libmamba --quiet --yes --file requirements.txt
    - conda activate release
    - bash -e scripts/release-description.sh > release.md
    - cat release.md
  artifacts:
    expose_as: Release notes
    paths:
      - release.md

create release:
  extends:
    - .conda:base
    - .cache
  image: igwn/base:conda
  stage: release
  variables:
    # we need all branches
    GIT_DEPTH: 0
    GIT_STRATEGY: "clone"
  rules:
    # can't create a new tag from a tag
    - if: '$CI_PROJECT_PATH == "computing/conda" && $CI_COMMIT_TAG'
      when: never
    # only create a new tag from the staging branch of the main repo
    - if: '$CI_PROJECT_PATH == "computing/conda" && $CI_COMMIT_BRANCH == "staging"'
      when: manual
      variables:
        ARGUMENTS: "--fail-no-diff"
        RELEASE_BRANCH: "${CI_COMMIT_BRANCH}"
        REMOTE: "autotag"
    # for MRs against the staging branch, execute with --dry-run
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event" && $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "staging"'
      when: always
      variables:
        ARGUMENTS: "--dry-run"
        RELEASE_BRANCH: "${CI_MERGE_REQUEST_TARGET_BRANCH_NAME}"
        REMOTE: "${CI_PROJECT_PATH}"
  before_script:
    - !reference [".conda:base", before_script]
    - *define-retry
    - retry conda create --name release --solver libmamba --quiet --yes --file requirements.txt
    - conda activate release
  script:
    # add git config to enable tag creation
    - git config user.name "${GITLAB_USER_NAME}"
    - git config user.email "${GITLAB_USER_EMAIL}"
    # configure access using 'autotag' project access token
    - if [[ "${REMOTE}" == "autotag" ]]; then
          git remote add __autotag__ https://autotag:${AUTOTAG_TOKEN}@git.ligo.org/${CI_PROJECT_PATH};
          git fetch __autotag__;
      fi
    - set -- ${ARGUMENTS:-}
    - python ./scripts/release.py
          --branch "${RELEASE_BRANCH}"
          --remote "${REMOTE}"
          "$@"
  allow_failure:
    exit_codes: 3

release:
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  rules:
    # only for stable releases
    - *if-stable
  needs:
    - release-notes
  script:
    - release-cli create
          --name "IGWN Conda Distribution release for $CI_COMMIT_TAG"
          --description ./release.md
