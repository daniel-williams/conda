# -*- coding: utf-8 -*-
# Copyright (C) 2022 Cardiff University

__author__ = "Duncan Macleod <duncan.macleod@ligo.org>"

import logging

try:
    from coloredlogs import ColoredFormatter as Formatter
except ModuleNotFoundError:
    from logging import Formatter

LOGGER_FORMAT = "[%(asctime)-15s] %(name)s | %(levelname)+8s | %(message)s"
LOGGER_DATEFMT = "%Y-%m-%d %H:%M:%S"


def create_logger(name, level=logging.INFO):
    """Return the `logging.Logger` with the given name, creating if needed.
    """
    logger = logging.getLogger(f"igwn-conda | {name}")
    logger.setLevel(level)
    if not logger.handlers:
        handler = logging.StreamHandler()
        handler.setFormatter(Formatter(
            fmt=LOGGER_FORMAT,
            datefmt=LOGGER_DATEFMT,
        ))
        logger.addHandler(handler)
    return logger
