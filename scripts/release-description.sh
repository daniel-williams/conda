#!/bin/bash
#
# Render the 'description' for a gitlab release for the
# new stable release of the IGWN Conda Distribution
#

set -e
set -o pipefail

OLD_TAG=$(git tag --list | tail -n2 | head -n1)
if [[ ! -z "${CI_MERGE_REQUEST_TARGET_BRANCH_NAME+x}" ]]; then
  # for merge requests, just report from HEAD
  NEW_TAG=""
  DATE_STR="THIS MERGE REQUEST"
else
  NEW_TAG=$(git tag --list | tail -n1)
  DATE_STR=$(date -d ${NEW_TAG} +"%d %B %Y")
fi

DIFF=$(python scripts/diff.py ${OLD_TAG} ${NEW_TAG} | tail -n +3)

cat <<EOF
The ${NEW_TAG} tag represents the stable release of the IGWN Conda Distribution
for ${DATE_STR}.

For a full diff between old and new environments please refer to the git
history, or \`conda list\`.

**Summary of changes relative to ${OLD_TAG}**:

[[_TOC_]]

:zap: These changes only refer to the Linux environments, for macOS or Windows
please refer to the git history, or online documentation.

${DIFF}
EOF
