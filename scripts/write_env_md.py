#!/usr/bin/env python

"""Format the current list of environments for inclusion in the docs
"""

import json
import os
import subprocess
import sys
import tempfile
from operator import itemgetter
from pathlib import Path

import yaml

from tabulate import tabulate

ANACONDA_CHANNELS = {
    "pkgs/free",
    "pkgs/main",
}

BASEDIR = Path(__file__).resolve().parent.parent
SUBDIR = os.getenv(
    "SUBDIR",
    os.getenv("CONDA_SUBDIR", "linux-64"),
)


def find_subdirs(path=BASEDIR / "environments"):
    """Yield subdirs supported by this distribution.
    """
    for subdir in Path(path).glob("*"):
        if (
            subdir.name.startswith(("linux", "osx", "win"))
            and subdir.is_dir()
        ):
            yield subdir.stem


def default_environment(config_file=None):
    """Parse the default-environment key from the given YAML file
    """
    with open(config_file or BASEDIR / "igwn_conda_config.yaml", "r") as file:
        return yaml.load(file, Loader=yaml.SafeLoader)['default-environment']


def find_environments(path=None):
    if path is None:
        path = BASEDIR / "environments" / SUBDIR
    versioned = sorted(Path(path).rglob("*.yaml"))
    default = default_environment()
    return {
        "igwn":
            path / default / f"{default}.yaml",
        "igwn-testing":
            path / f"{default}-testing" / f"{default}-testing.yaml",
        "igwn-staging":
            path / f"{default}-staging" / f"{default}-staging.yaml",
    } | {env.stem: env for env in versioned}


def write_environment(path, name, file, subdirs):
    path = Path(path)
    with path.open("rb") as fobj:
        env = yaml.load(fobj, Loader=yaml.SafeLoader)

    if name is None:
        name = env['name']

    # render environment
    prefix = tempfile.mktemp(prefix=f"render-{name}")
    cmd = [
        "mamba",
        "create",
        "--json",
        "--dry-run",
        "--prefix", prefix,
    ] + env["dependencies"]
    rendered = json.loads(subprocess.check_output(cmd))

    # write name
    print(f"# {name}\n", file=file)
    print(f"**Source:** {env['name']}  ", file=file)

    # write link
    links = ", ".join(
        f"[{subdir}]({subdir}/{path.name})"
        for subdir in subdirs
    )
    print(f"**Download:** {links}  ", file=file)
    channels = env['channels']
    if 'nodefaults' not in channels:
        channels.insert(0, 'defaults')
    print(
        "**Channels:** `{0}`  ".format("`, `".join(channels)),
        file=file,
    )
    print(
        f"""**Install:**

```
conda env create --file {path.name}
```""",
        file=file,
    )
    print(
        f"""**Activate:**

```
conda activate {name}
```""",
        file=file,
    )

    # write packages
    print(
        f"""**Packages:**

!!! note "Package list for {SUBDIR}"
    The following package table describes the environment contents
    on **{SUBDIR}**, see the above **Download** links for environment
    YAML files for all supported platforms.
""",
        file=file,
    )
    rows = []
    for pkg in sorted(rendered["actions"]["LINK"], key=itemgetter("name")):
        cloudchannel = ('anaconda' if pkg['channel'] in ANACONDA_CHANNELS else
                        pkg['channel'])
        cloudurl = f"https://anaconda.org/{cloudchannel}/{pkg['name']}/"
        rows.append((
            f"[{pkg['name']}]({cloudurl})",
            pkg['version'],
            f"`{pkg['build_string']}`",
            pkg['channel'],
        ))
    table = tabulate(
        rows,
        headers=('Name', 'Version', 'Build', 'Channel'),
        tablefmt="github",
    )
    print(table, file=file)


def main(verbose=False):
    # find subdirs
    subdirs = sorted(find_subdirs())
    if verbose:
        print("Discovered the following subdirs:")
        for subdir in subdirs:
            print(f"    {subdir}")
    # copy all environment files to output
    for name, path in find_environments().items():
        target = Path("docs") / "environments" / (name + '.md')
        if verbose:
            print(f"Processing {name}")
        with open(target, 'w') as envf:
            write_environment(path, name, envf, subdirs)


if __name__ == "__main__":
    sys.exit(main(verbose=True))
