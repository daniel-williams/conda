#!/bin/bash
#
# Authenticate with kerberos and ECP (X.509)
#

set -e

# -- helpers ----------------

# parse the keytab from a variable and write to a file
unpack_keytab() {
    local input=$1
    local target=$2
    echo "${input}" | base64 -d > "${target}"
    chmod 0600 "${target}"
}

# get the principal name from the keytab file
get_principal() {
    local keytab=$1
    klist -k "${keytab}" | tail -n 1 | awk '{print $2}'
}

# -- run --------------------

KEYTAB_FILE=".keytab"

# create the keytab file and extract the principal name
unpack_keytab "${ROBOT_LIGO_KEYTAB}" ${KEYTAB_FILE}
ROBOT_PRINCIPAL=$(get_principal ${KEYTAB_FILE})

# create a new kerberos token
kinit "${ROBOT_PRINCIPAL}" -k -t "${KEYTAB_FILE}"
klist  # print the kerberos credential info

# use the kerberos token to get a new X.509 certificate
ecp-get-cert -i login.ligo.org -k
ecp-cert-info  # print the X.509 credential info
