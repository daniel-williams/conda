#!/usr/bin/env python3

"""Create a new stable release of the IGWN Conda Distribution,
based on today's date
"""

import argparse
import glob
import os
import smtplib
import sys
from datetime import datetime
from email.message import EmailMessage
from email.policy import SMTP
from pathlib import Path

import git
import yaml

from log import create_logger

CANONICAL_REMOTE = os.getenv(
    "CI_MERGE_REQUEST_PROJECT_PATH",
    os.getenv(
        "CI_PROJECT_PATH",
        "computing/conda",
    ),
)
STAGING_BRANCH = "staging"

# files to ignore when determining whether a diff is present
CHANGES_IGNORE = (
    ".git*",
    "LICENSE",
    "README.md",
    "mkdocs.yml",
    "*requirements.txt",
    "docs/**",
    "scripts/*.py",
)


def default_env(configfile):
    """Return the name of the default environment.
    """
    with open(configfile, "r") as file:
        return yaml.load(file, Loader=yaml.SafeLoader)['default-environment']


def tag_from_date(repo, date):
    """Determine the name of the new tag for the given date
    """
    tagdate = date.strftime("%Y%m%d")
    current = []
    for tag in repo.tags:
        if tag.name.startswith(tagdate):
            current.append(tag.name)
    if current:
        try:
            point = int(max(current).split(".", 1)[1])
        except IndexError:
            point = 0
        return f"{tagdate}.{point+1}"
    return tagdate


def identify_remote(repo, path):
    """Return the name of remote tracking the given remote path.
    """
    for remote in repo.remotes:
        if path in remote.url:
            return remote
    raise RuntimeError(
        f"failed to identify remote tracking {path}"
    )


def diff(a, b):
    changes = {p for d in a.diff(b) for p in (d.a_path, d.b_path)}
    allfiles = set(a.repo.git.ls_files().splitlines())
    ignored = {f for pat in CHANGES_IGNORE for f in glob.glob(pat)}
    keep = allfiles - ignored
    return bool(changes & keep)


def announce(
    tag,
    defaultenv,
    concreteenv,
    to=("ldg-announce@ligo.org",),
    from_="compsoft@ligo.org",
    smtp_server=None,
    dry_run=False,
):
    """Announce this release via email.
    """
    message = EmailMessage(policy=SMTP)
    message["To"] = ", ".join(to)
    message["From"] = from_
    message["Subject"] = f"IGWN Conda Distribution release: {tag}"
    message.set_content(f"""Dear all,

The IGWN Conda Distribution has been updated with a new environment set dated {tag}.
The '{defaultenv}' environment in CVMFS will be updated to link to the {concreteenv} environment;
these changes should be visible in CVMFS in a few hours.

For a summary of the changes for each stable environment, please see the release notes here:

https://git.ligo.org/computing/conda/-/releases/{tag}

For a full diff between old and new environments please refer to 'conda list'.

If you have any questions, please post a ticket on the Computing Helpdesk

https://git.ligo.org/computing/helpdesk/-/issues/

or for documentation of the IGWN Conda Distribution itself, including full listings of the current stable, staging, and testing environments, please see

https://computing.docs.ligo.org/conda/


Thanks
The IGWN Conda Distribution team""")  # noqa: E501

    # send the message (if we can)
    if not dry_run and smtp_server:
        with smtplib.SMTP(smtp_server) as server:
            server.send_message(message)

    return message


def create_parser():
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        "-b",
        "--branch",
        default=STAGING_BRANCH,
        help="Name of branch from which to tag",
    )
    parser.add_argument(
        "-c",
        "--igwn-config-file",
        default="igwn_conda_config.yaml",
        type=Path,
    )
    parser.add_argument(
        "--dry-run",
        action="store_true",
        help=(
            "Do not actually create tags and push, just print what "
            "would have been done. "
            "--dry-run stills creates/updates local branches."
        ),
    )
    parser.add_argument(
        "--fail-no-diff",
        action="store_true",
        help="Exit with code 3 if no relevant diff is found (otherwise code 0)",
    )
    parser.add_argument(
        "-r",
        "--remote",
        default=CANONICAL_REMOTE,
        help="Path of remote to push to",
    )
    parser.add_argument(
        "-s",
        "--smtp-server",
        help="FQDN of SMTP server to use when sending email announcement.",
    )
    return parser


def main(args=None):
    parser = create_parser()
    args = parser.parse_args()

    logger = create_logger("release", level="DEBUG")
    info = logger.info
    debug = logger.debug
    warning = logger.warning
    critical = logger.critical
    if args.dry_run:
        info("THIS IS A DRY RUN")

    repo = git.Repo()

    # get tag from current date
    now = datetime.utcnow()
    tag = tag_from_date(repo, now)
    debug(f"Tag identified: {tag}")

    # checkout correct tag target
    info(
        f"Checking out (up-to-date) {args.branch} branch from {args.remote}",
    )
    remote = identify_remote(repo, args.remote)
    debug(f"Using remote '{remote}'")
    debug(f"Fetching {remote}/{args.branch} from {remote.url}")
    remote.fetch(f"{args.branch}:{args.branch}")

    # find commit to tag and verify diff
    debug(f"Identifying commit to tag")
    branch = repo.branches[args.branch]
    target = branch.commit
    if not diff(repo.tags[-2].commit, target):
        msg = "No relevant diff from previous tag"
        if args.fail_no_diff:
            critical(msg)
            return 3
        warning(f"{msg}, stopping here")
        return

    # create new tag
    info(f"Tagging {target} (HEAD of {branch}) as {tag}")
    if os.getenv("CI") or not args.dry_run:
        tagobj = repo.create_tag(
            tag,
            target,
            message=(
                "IGWN Conda Distribution release for "
                f"{now.strftime('%Y %m %d')}"
            ),
        )

    # push
    info(f"Pushing to {remote.url}")
    if not args.dry_run:
        remote.push(
            tagobj.path,
            signed="if-asked",
            verbose=True,
            verify=True,
        )

    concreteenv = f"{default_env(args.igwn_config_file)}-{tag}"
    default = concreteenv.split("-")[0]
    email = announce(
        tag,
        default,
        concreteenv,
        dry_run=args.dry_run,
        smtp_server=args.smtp_server,
    )
    if args.smtp_server:
        info("Email announcement sent")
    else:
        warning("No SMTP server configured, email announcement not sent")
        warning("Please send the following email manually")
    debug(f"Email contents:\n{email.as_string().strip()}")


if __name__ == "__main__":
    sys.exit(main())
