# vim: set ft=dockerfile :
# Build IGWN Conda distribution using Docker
#
# Copyright (c) 2019 Duncan Macleod <duncan.macleod@ligo.org>
# This file is licensed under the terms of the MIT license,
# see LICENSE for full terms.
#
# This Dockerfile creates a container that should not
# be used for anything, it exists purely to provide a pristine
# build environment in which to build out the IGWN Conda environments.
#
# Conda requires that the environments are created using the production
# runtime path, which is
#
# /cvmfs/software.igwn.org/conda
#
# so we build them using `docker build` then unpack them using
# `COPY --from=builder ${INSTALL_PATH} /` so as to upload them
# to the CVMFS repo host and publish them.

# -- step 1: build the environments in their production location --------------

FROM debian:buster AS builder

ARG INSTALL_PATH=/cvmfs/software.igwn.org/conda/
ARG SUBDIR=linux-64
ARG BUILD_OPTS=""

ENV LANG=C.UTF-8 LC_ALL=C.UTF-8
ENV PATH=${INSTALL_PATH}/condabin:${PATH}

RUN apt-get update --quiet --fix-missing && \
    apt-get install --quiet --quiet --yes \
      "bash" \
      "curl" \
      "libglib2.0-0"

# install mambaforge and configure the base environment
COPY igwn_conda_config.yaml /tmp/
RUN mkdir -p $(dirname ${INSTALL_PATH}) && \
    curl --location --silent \
      https://github.com/conda-forge/miniforge/releases/latest/download/Mambaforge-$(uname)-$(uname -m).sh \
      --output /root/mambaforge.sh && \
    /bin/bash /root/mambaforge.sh -b -p ${INSTALL_PATH} && \
    rm -f /root/mambaforge.sh && \
    conda config --system --append channels igwn && \
    conda config --system --set solver libmamba && \
    ${INSTALL_PATH}/bin/python -c "import ruamel.yaml; yaml = ruamel.yaml.YAML(); config = yaml.load(open('/tmp/igwn_conda_config.yaml', 'r')); print(' '.join(config['base-packages']))" | xargs -t conda install --solver libmamba --yes --satisfied-skip-solve && \
    conda info --all && \
    conda list --name base && \
    conda clean -afy && \
    touch -a $(conda info --base)/.cvmfscatalog

# copy rendered environments
COPY environments/${SUBDIR} /tmp/environments
COPY scripts/*.py /tmp/
RUN DEFAULT_ENVIRONMENT=$(${INSTALL_PATH}/bin/python -c "import ruamel.yaml; yaml = ruamel.yaml.YAML(); config = yaml.load(open('/tmp/igwn_conda_config.yaml', 'r')); print(config['default-environment'])") && \
    ${INSTALL_PATH}/bin/python \
      "/tmp/build.py" \
      "/tmp/environments" \
      --conda "${INSTALL_PATH}/condabin/conda" \
      --default-environment "${DEFAULT_ENVIRONMENT}" \
      --envs-dir "${INSTALL_PATH}/envs" \
      --cvmfs \
      --verbose \
      ${BUILD_OPTS} && \
    conda clean -afy

# -- step 2: unpack them into / to act as a tarball ---------------------------

FROM scratch
ARG INSTALL_PATH=/cvmfs/software.igwn.org/conda/
COPY --from=builder ${INSTALL_PATH} /
