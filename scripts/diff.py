#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright (C) 2022 Cardiff University

"""Format a diff between two tags
"""

__author__ = "Duncan Macleod <duncan.macleod@ligo.org>"

import argparse
import io
import re
import sys
from collections import defaultdict

from packaging.version import parse as parse_version

import git

# local module
import utils

IGWN_CONFIG = "igwn_conda_config.yaml"
DIFF_PATHS = [
    IGWN_CONFIG,
    "packages/",
]
REPO = git.Repo()
VERSION_REGEX = re.compile("({})".format("|".join((
    "<=",
    "<",
    ">",
    ">=",
    "=",
))))

FOOTNOTE = {
    "removed": (
        "Packages whose specifications have been removed may still be "
        "present in the distribution, but their versions are not only "
        "constrained by the requirements of other packages."
    ),
    "relaxed": (
        "{unpinned} here means that the package is guaranteed to be present, "
        "but it's version is only contrained by the requirements of other "
        "packages."
    ),
}


class PackageChange:
    __slots__ = ("name", "old", "new", "type")

    def __init__(self, name, old, new):
        self.name = name
        self.old = old
        self.new = new
        self.type = self._change_type(old, new)

    @staticmethod
    def _change_type(old, new):
        if old is None:
            return "added"
        if new is None:
            return "removed"
        old = old.lstrip("<>=")
        new = new.lstrip("<>=")
        if (
            new == "{unpinned}"
            or (old != new and old.startswith(new))
        ):
            return "relaxed"
        if (
            old == "{unpinned}"
            or (new != old and new.startswith(old))
        ):
            return "tightened"
        old = parse_version(old)
        new = parse_version(new)
        if old == new:
            return "rebuilt"
        if old < new:
            return "upgraded"
        return "downgraded"


def create_parser():
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        "refs",
        nargs="*",
        help="refs to diff; if not given this will be the two "
             "most recent tags, if only one is given this will be "
             "base ref against which to diff HEAD",
    )
    return parser


def get_refs(*refs):
    """Format a user request into a start, stop pair of git references
    """
    # nothing given by user, and current HEAD is a tag
    if not refs and REPO.head.commit == REPO.tags[-1].commit:
        # compare last two tags
        return REPO.tags[-2], REPO.tags[-1]
    # nothing given by user, HEAD is not a tag
    if not refs:
        # compare HEAD to the last tag
        return REPO.tags[-1], REPO.head
    # one ref given by user
        # compare HEAD to ref
    if len(refs) == 1:
        return REPO.refs[refs[0]], REPO.head
    # both refs given by user, compare them
    return REPO.refs[refs[0]], REPO.refs[refs[1]]


def read_blob_yaml(blob, **context):
    """Read YAML data from a git blob
    """
    return utils.parse_yaml(
        io.BytesIO(blob.data_stream.read()).read().decode("utf-8"),
        **context,
    )


def parse_python_versions(old, new, **context):
    """Parse python versions from the igwn_conda_config.yaml
    across two git references

    Returns
    -------
    removed : `list`
        the list of removed python versions
    current : `list`
        the list of current python versions
    """
    def _versions(conf):
        for pyv in sorted(conf["python"]):
            yield ".".join(pyv.split(",", 1)[0].split(".")[:2])

    # read current python versions from config file
    with open(IGWN_CONFIG, "r") as conf:
        current = set(_versions(utils.parse_yaml(conf.read(), **context)))

    # read old python versions by parsing the diff
    diffs = old.commit.diff(
        new.commit,
        paths=[IGWN_CONFIG],
    )
    try:
        configdiff, = diffs
    except ValueError:  # no changes
        old = set()
    else:
        old = set(_versions(read_blob_yaml(configdiff.a_blob, **context)))
    return sorted(old - current), current


def _parse_new_package(blob, **context):
    """Parse a new package as a `PackageChange`
    """
    package = read_blob_yaml(blob, **context)['package']
    return PackageChange(package['name'], None, package.get('version', "{unpinned}"))


def _parse_deleted_package(blob, **context):
    """Parse a deleted packages as a `PackageChange`
    """
    package = read_blob_yaml(blob, **context)['package']
    return PackageChange(package['name'], package.get('version', "{unpinned}"), None)


def parse_package_diff(diff, **context):
    """Parse a modified package as a `PackageChange`

    This returns a list of one item
    """
    if diff.change_type == "A":
        return [_parse_new_package(diff.b_blob, **context)]
    if diff.change_type == "D":
        return [_parse_deleted_package(diff.a_blob, **context)]

    old = read_blob_yaml(diff.a_blob, **context)
    new = read_blob_yaml(diff.b_blob, **context)
    if new.get("skip", False) or old["package"] == new["package"]:
        return []
    return [PackageChange(
        new["package"]['name'],
        (
            None if old.get("skip", False)
            else old["package"].get("version", "{unpinned}")
        ),
        new["package"].get('version', "{unpinned}"),
    )]


def parse_config_diff(diff, **context):
    """Return `PackageChange` instances from a diff of igwn_conda_config.yaml

    This returns a list of one item
    """
    py = str(context.get("py", utils.DEFAULT_YAML_CONFIG["py"]))
    pyv = "{}.{}".format(py[0], py[1:])
    old = read_blob_yaml(diff.a_blob, **context)['python']
    new = read_blob_yaml(diff.b_blob, **context)['python']

    # if the python versions haven't changed, we don't care
    if old == new:
        return []

    for version in new:
        if not version.startswith(pyv):
            continue
        xdoty = version.split(",", 1)[0].split(".", 2)[:2]
        for pkg2 in old:
            v2 = pkg2.split(",")[0]
            # if the minor version isn't what we're looking for, carry on
            if v2.split(".", 2)[:2] != xdoty:
                continue
            # if the version has changed, report that
            if version != v2:
                return [PackageChange("python", v2, version)]
        # if we got here this version is new
        return [PackageChange("python", None, version)]


def _envlist_to_dict(env):
    pkgs = {}
    for pkg in env:
        match = VERSION_REGEX.split(pkg, 1)
        try:
            name, op, vers = match
        except ValueError:  # no version spec
            pkgs[pkg] = "{unpinned}"
        else:
            pkgs[name.strip()] = f"{op}{vers}".lstrip("=")
    return pkgs


def parse_upstream_diff(diff, **context):
    """Yield `PackageChange` instances from the diff of the upstream.yaml
    """
    old = read_blob_yaml(diff.a_blob, **context)['dependencies']
    new = read_blob_yaml(diff.b_blob, **context)['dependencies']
    oldpkgs = _envlist_to_dict(old)
    newpkgs = _envlist_to_dict(new)
    removed = set(oldpkgs) - set(newpkgs)
    for pkg, version in newpkgs.items():
        if pkg not in oldpkgs:
            yield PackageChange(pkg, None, version)
        if pkg in oldpkgs and version != oldpkgs[pkg]:
            yield PackageChange(pkg, oldpkgs[pkg], version)
    for pkg in removed:
        yield PackageChange(pkg, oldpkgs[pkg], None)


def print_changes(changes):
    """Print a markdown-formatted summary of the changes
    """
    groups = defaultdict(list)
    for change in changes:
        groups[change.type].append(change)

    def _print_markdown_table_header(headers, file=sys.stdout):
        _print_markdown_table_row(headers, file=file)
        _print_markdown_table_row(
            [re.sub(r"[\w\s]", "-", h) for h in headers],
            file=file,
        )

    def _print_markdown_table_row(data, file=sys.stdout):
        print("| {} |".format(" | ".join(data)), file=file)

    column_names = {
        "name": "Package name",
        "old": "Previous version",
        "new": "Current version",
    }

    for group, cols in (
        ("added", ("name", "new")),
        ("removed", ("name", "old")),
        ("upgraded", ("name", "old", "new")),
        ("downgraded", ("name", "old", "new")),
        ("relaxed", ("name", "old", "new")),
        ("tightened", ("name", "old", "new")),
    ):
        if groups[group]:
            note = f"[^{group}]" if group in FOOTNOTE else ""
            print(
                "\nThe following package specifications were "
                f"{group}{note}:\n",
            )
            _print_markdown_table_header([column_names[col] for col in cols])
            for change in sorted(groups[group], key=lambda x: x.name):
                data = [getattr(change, col) for col in cols]
                _print_markdown_table_row(data)


PARSER = {
    "igwn_conda_config.yaml": parse_config_diff,
    "packages/upstream.yaml": parse_upstream_diff,
}


def diff_python_version(pyv, old, new):
    """Find and yield all changes for the given python X.Y version
    """
    xy = "".join(pyv.split(",", 1)[0].split(".")[:2])
    for diff in old.commit.diff(new.commit, paths=DIFF_PATHS):
        if not diff.a_path.endswith((".yml", ".yaml")):  # only check YAML
            continue
        parser = PARSER.get(diff.b_path, parse_package_diff)
        for mod in parser(diff, py=int(xy)):
            yield mod


def diff(*references):
    """Format a markdown report for the diff between two references
    """
    old, new = get_refs(*references)
    print(f"Comparing {old} and {new}\n")

    # find the environment prefix
    with open(IGWN_CONFIG, "r") as f:
        prefix = utils.parse_yaml(f.read())["prefix"]

    # parse the differences in python versions
    removed, current = parse_python_versions(old, new)

    # print a statement about removed versions
    for version in removed:
        xy = "".join(version.split(".")[:2])
        print(fr"""## `{prefix}py{xy}`

Support for the IGWN Python {version} environments has ended and
these environments will be removed in the near future.""")

    # print differences for current versions
    for version in sorted(current):
        xy = "".join(version.split(".")[:2])
        print("")
        print(fr"## `{prefix}py{xy}`")
        changes = diff_python_version(version, old, new)

        print_changes(changes)
        print("\n[Back to top](#)")

    print("\nNotes:\n")
    for group, note in FOOTNOTE.items():
        print(f"[^{group}]: {note}")


def main(args=None):
    """Run the diff tool
    """
    parser = create_parser()
    args = parser.parse_args(args=args)
    return diff(*args.refs)


if __name__ == "__main__":
    sys.exit(main())
