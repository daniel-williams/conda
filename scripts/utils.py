# -*- coding: utf-8 -*-
# Copyright (C) 2022 Cardiff University

__author__ = "Duncan Macleod <duncan.macleod@ligo.org>"

import jinja2

from conda_build.config import Config
from conda_build.metadata import (ns_cfg, select_lines, yamlize)

# -- YAML parsing -------------------------------

# YAML jinja2 variables
CONDA_BUILD_CONFIG = Config()
DEFAULT_YAML_CONFIG = ns_cfg(CONDA_BUILD_CONFIG)


def parse_yaml(data, **config):
    """Read a YAML file from a path
    """
    namespace = DEFAULT_YAML_CONFIG.copy()
    namespace.update(config)
    # render the YAML file using jinja2 to resolve internal variable usage
    data = jinja2.Template(data).render(**namespace)
    # evaluate selectors and return
    return yamlize(select_lines(data, namespace, False))


def igwn_python_versions(config_file):
    for pyv in parse_yaml(open(config_file).read())['python']:
        yield '{0[0]}.{0[1]}'.format(pyv.split('.'))
