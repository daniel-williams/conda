<!--
Please complete this form as fully as you can, so that your request can be evaluated in a timely manner.
-->
#### Details

- **Package name:** <!-- insert package name -->
- **Version:** <!-- insert specific package version -->

#### Description

<!--
Insert reasoning for request here

In particular, include a description of the requested package, and why is is required.
Please also try and detail what downstream packages/groups may be impacted by your request, either negatively or positively.

**If this is a package update request, please link to the relase notes or changelog for the updated version**
-->

#### Python versions

Please select the Python versions appropriate for this package (deselect those that are not required):

- [x] 2.7
- [x] 3.7

#### Request type

Please select all of the following that apply:

- [ ] this is a new package
- [ ] this is a backwards-compatible update
- [ ] this is a backwards-incompatible update [API/ABI changes]
